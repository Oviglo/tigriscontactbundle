<?php

namespace Tigris\ContactBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ContactBundle\Repository\MessageRepository;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ORM\Table(name: 'contact_message')]
class Message
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(type: Types::JSON)]
    #[Gedmo\Translatable]
    private array $data = [];

    #[ORM\Column(length: 20, nullable: true)]
    private string|null $ip = null;

    #[ORM\Column]
    private string $form;

    public function __construct()
    {
        $this->ip = Utils::getIp();
    }

    public function getId(): null|int
    {
        return $this->id;
    }

    public function getData($name = null): mixed
    {
        if ($name) {
            return $this->data[$name] ?? null;
        }

        return $this->data;
    }

    public function getDataFormat(mixed $data): string
    {
        if (is_object($data)) {
            if ($data instanceof \DateTime) {
                return 'datetime';
            }

            return 'object';
        }

        return 'string';
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIp(): null|string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getForm(): string
    {
        return $this->form;
    }

    public function setForm(string $form): self
    {
        $this->form = $form;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getEmail(): ?string
    {
        if (null !== $this->getData('email')) {
            return (string) $this->getData('email');
        }

        return null;
    }
}
