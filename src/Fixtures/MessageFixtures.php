<?php

namespace Tigris\ContactBundle\Fixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Attribute\When;
use Tigris\ContactBundle\Entity\Message;

#[When(env: 'dev')]
#[When(env: 'test')]
class MessageFixtures extends Fixture
{
    private array $messages = [
            ['form' => 'contact_form', 'data' => ['message' => 'Je suis mon cher ami, très heureux de te voir'], 'createdAt' => 'now'],
            ['form' => 'contact_form', 'data' => ['message' => 'Il ne faut jamais parler sèchement à un Numide'], 'createdAt' => '24 hours ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'Il semblerait qu’Amora, déesse de la moutarde, soit montée au nez des autres dieux'], 'createdAt' => '3 days ago 2 hours ago 15 minutes ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'Tous les étés les Ibères deviennent plus rudes'], 'createdAt' => '4 months ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'L’arrière-garde n’a pas fait son travail, ce qui a transformé l’avant garde en arrière-garde. Retourne en arrière et en avant !'], 'createdAt' => 'last day of 2 months ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'Mon petit bonhomme, les phrases historiques, aléa jacta est et tout ça, c’est moi qui les fais ici !'], 'createdAt' => '7 days ago 24 minutes ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'Allons, reprends du poil de l’homme ! Tu te conduis comme un jeune marcassin qui aurait encore des dents de laie.'], 'createdAt' => '1 year ago 5 hours ago 23 minutes ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'Moi, je suis tranquille ! Jamais une femme ne pourra me remplacer. Tailler des menhirs, c’est un travail bien trop délicat !'], 'createdAt' => '1 month ago 3 days ago 10 minutes ago'],
            ['form' => 'contact_form', 'data' => ['message' => 'Bon ! Maintenant, écartons nous ! Quand Asterix boit, certains vont trinquer !'], 'createdAt' => '1 month ago 6 days ago 40 minutes ago'],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->messages as $message) {
            $entity = (new Message())
                ->setCreatedAt(new \DateTime($message['createdAt']))
                ->setForm($message['form'])
                ->setData($message['data'])
            ;

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
