<?php

namespace Tigris\ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Form\Type\RecaptchaType;
use Tigris\BaseBundle\Form\Type\SecurityButtonType;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Validator\Recaptcha;

class MessageType extends AbstractType
{
    public function __construct(private readonly ConfigManager $configManager) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Honeypot
        $builder
            ->add('myemail', Type\EmailType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'd-none',
                ],
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new Assert\Blank(),
                ],
            ])

            ->add('securityButton', SecurityButtonType::class)
        ;

        if ($this->configManager->getValue('TigrisContactBundle.google_recaptcha_enable', false)) {
            $builder
                ->add('recaptcha', RecaptchaType::class, [
                    'constraints' => [
                        new Recaptcha(),
                    ],
                ])
            ;
        }
    }
}
