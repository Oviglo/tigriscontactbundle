<?php

namespace Tigris\ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Tigris\BaseBundle\Form\Type\TableCollectionType;

class ConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('google_recaptcha_enable', Type\CheckboxType::class, [
                'label' => 'config.google_recaptcha.enable',
                'required' => false,
                'constraints' => [],
            ])

            ->add('recipients_emails', TableCollectionType::class, [
                'label' => 'config.recipients_emails',
                'entry_type' => Type\EmailType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'add_button_text' => 'button.plus_min',
                'delete_button_text' => 'button.delete_min',
            ])
        ;

        $builder->get('google_recaptcha_enable')
            ->addModelTransformer(new CallbackTransformer(
                fn ($valueAsString) => 1 == (int) $valueAsString,
                fn ($valueAsBoolean) => $valueAsBoolean
            ))
        ;
    }
}
