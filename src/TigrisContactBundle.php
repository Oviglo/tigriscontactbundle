<?php

namespace Tigris\ContactBundle;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class TigrisContactBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $parameters = $container->parameters()
            ->set('tigris_contact', $config)
        ;

        foreach ($config as $key => $value) {
            $parameters->set("tigris_contact.$key", $value);
        }

        $container->import(__DIR__.'/../config/services.yaml');
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('forms')
                    ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                        ->scalarNode('name')->end()
                        ->arrayNode('fields')
                            ->useAttributeAsKey('id')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('type')->end()
                                        ->scalarNode('label')->end()
                                        ->booleanNode('required')->defaultFalse()->end()
                                        ->booleanNode('expanded')->end()
                                        ->scalarNode('class')->end()
                                        ->booleanNode('multiple')->end()
                                        ->arrayNode('choices')
                                            ->requiresAtLeastOneElement()
                                            ->useAttributeAsKey('value')
                                            ->prototype('scalar')->end()
                                        ->end()
                                        ->arrayNode('options')
                                            ->requiresAtLeastOneElement()
                                            ->useAttributeAsKey('value')
                                            ->prototype('scalar')->end()
                                        ->end()
                                        ->arrayNode('attr')
                                            ->requiresAtLeastOneElement()
                                            ->useAttributeAsKey('value')
                                            ->prototype('scalar')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
