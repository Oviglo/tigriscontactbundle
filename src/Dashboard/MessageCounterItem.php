<?php

namespace Tigris\ContactBundle\Dashboard;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Tigris\BaseBundle\Contracts\Dashboard\DashboardItemInterface;
use Tigris\BaseBundle\Dashboard\ItemConfigBuilder;
use Tigris\BaseBundle\Dashboard\ItemDataBuilder;
use Tigris\BaseBundle\Dashboard\Location;
use Tigris\BaseBundle\Dashboard\Type;
use Tigris\ContactBundle\Repository\MessageRepository;

#[AutoconfigureTag('tigris_base.dashboard_item')]
class MessageCounterItem implements DashboardItemInterface
{
    public function __construct(private readonly MessageRepository $messageRepository) {}

    public function configure(): array
    {
        return (new ItemConfigBuilder())
            ->id('contact-messages')
            ->icon('envelope-open-text')
            ->title('dashboard.contact_message')
            ->route('tigris_contact_admin_contact_index')
            ->placement(Location::HEADER, 0)
            ->type(Type::ICON_COUNTER)
            ->toArray()
        ;
    }

    public function data(): array
    {
        return (new ItemDataBuilder())
            ->value($this->messageRepository->count([]))
            ->toArray()
        ;
    }
}