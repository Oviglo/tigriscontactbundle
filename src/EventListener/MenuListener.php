<?php

namespace Tigris\ContactBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class MenuListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_ADMIN_MENU => 'onLoadAdminMenu',
        ];
    }

    public function onLoadAdminMenu(MenuEvent $event): void
    {
        $menu = $event->getMenu();

        $menu->addChild('menu.contact', [
            'route' => 'tigris_contact_admin_contact_index',
        ]);
    }
}
