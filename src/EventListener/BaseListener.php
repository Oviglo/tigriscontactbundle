<?php

namespace Tigris\ContactBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Event\StatsEvent;
use Tigris\BaseBundle\Utils\Stats;
use Tigris\ContactBundle\Form\Type\ConfigType;

class BaseListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_CONFIGS => 'onLoadConfigs',
            Events::LOAD_STATS => 'onLoadStats',
        ];
    }

    public function onLoadConfigs(FormEvent $event): void
    {
        $form = $event->getForm();
        $form->add('TigrisContactBundle', ConfigType::class, [
            'label' => 'config.contact',
        ]);

        $event->setForm($form);
    }

    public function onLoadStats(StatsEvent $event): void
    {
        $event->addStats(new Stats('menu.messages', 'tigris_contact_admin_contact_chart'));
    }
}
