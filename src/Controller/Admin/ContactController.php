<?php

namespace Tigris\ContactBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Controller\ChartControllerTrait;
use Tigris\BaseBundle\Form\Type\FormActionsType;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ContactBundle\Entity\Message;
use Tigris\ContactBundle\Repository\MessageRepository;

#[Route(path: '/admin/contact')]
#[IsGranted('ROLE_ADMIN')]
class ContactController extends BaseController
{
    use FormTrait;
    use ChartControllerTrait;

    #[Route(path: '/')]
    public function index(): Response
    {
        $this->generateBreadcrumbs([
            'menu.contact' => ['route' => 'tigris_contact_admin_contact_index'],
        ]);

        return $this->render('@TigrisContact/admin/message/index.html.twig');
    }

    #[Route(path: '/data', options: ['expose' => true])]
    public function data(Request $request, MessageRepository $messageRepository)
    {
        $criteria = $this->getCriteria($request);
        $data = $messageRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/chart', options: ['expose' => true])]
    public function chart(Request $request, MessageRepository $messageRepository, TranslatorInterface $translator): JsonResponse
    {
        $criteria = $this->getDateCriteria($request);

        $board = $this->getStatsBoard(
            $criteria['startDate'],
            $criteria['endDate'],
            false
        );
        $labels = $board['labels'];
        $stats = $board['stats'];
        $dateStart = $board['dates']['start'];
        $dateEnd = $board['dates']['end'];

        $entities = $messageRepository->findChartByDates($dateStart, $dateEnd);

        foreach ($entities as $entity) {
            $stats[(int) $entity['year'].'-'.(int) $entity['month']] = (int) $entity['nb'];
        }

        return new JsonResponse(['labels' => $labels,
            'stats' => [
                [
                    'label' => $translator->trans('contact.message.messages'),
                    'data' => array_values($stats),
                ],
            ],
        ]);
    }

    #[Route(path: '/{id}/show/', defaults: ['_format' => 'html'], options: ['expose' => true], requirements: ['id' => '\d+'])]
    public function show(Message $entity): Response
    {
        return $this->render('@TigrisContact/admin/message/show.html.twig', ['entity' => $entity]);
    }

    #[Route(path: '/remove', options: ['expose' => true], defaults: ['_format' => 'html'], methods: ['GET', 'DELETE'])]
    public function remove(Request $request, TranslatorInterface $translator, MessageRepository $messageRepository, EntityManagerInterface $em): Response
    {
        $selected = $request->query->get('ids', '');
        $ids = explode(',', $selected);
        $entities = $messageRepository->findByIds($ids);
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('tigris_contact_admin_contact_remove', ['ids' => $selected]))
            ->setMethod(Request::METHOD_DELETE)
            ->add('actions', FormActionsType::class, [
                'buttons' => [
                    'cancel' => [
                        'type' => ButtonType::class,
                        'options' => [
                            'as_link' => true,
                            'label' => 'button.cancel',
                            'attr' => ['data-cancel' => true, 'href' => $this->generateUrl('tigris_contact_admin_contact_index')],
                        ],
                    ],
                    'save' => [
                        'type' => SubmitType::class,
                        'options' => [
                            'label' => 'button.delete',
                            'attr' => ['class' => 'btn-danger'],
                        ],
                    ],
                ],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($entities as $entity) {
                $em->remove($entity);
            }

            $em->flush();

            return new JsonResponse(['status' => 'success', 'message' => $translator->trans('contact.message.delete.success')]);
        }

        return $this->render('@TigrisContact/admin/message/remove.html.twig', ['form' => $form, 'entities' => $entities]);
    }
}
