<?php

namespace Tigris\ContactBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\NotificationEvent;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Service\ArrayToFormService;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ContactBundle\Entity\Message;
use Tigris\ContactBundle\Form\Type\MessageType;

#[Route(path: '/contact', name: 'tigris_contact_message_')]
class ContactController extends BaseController
{
    use FormTrait;

    #[Route(path: '/form/{name}', name: 'index', requirements: ['name' => '[a-z_]+'], defaults: ['_format' => 'html', 'name' => 'contact_form'], methods: ['GET', 'POST'])]
    #[Route(path: '/form/{name}/new', name: 'new', requirements: ['name' => '[a-z_]+'], defaults: ['_format' => 'json', 'name' => 'contact_form'], methods: ['POST'])]
    public function form(
        Request $request,
        ArrayToFormService $arrayToFormService,
        ConfigManager $configManager,
        TranslatorInterface $translator,
        EventDispatcherInterface $eventDispatcher,
        UserRepository $userRepository,
        MailerInterface $mailer,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        string $name = 'contact_form'
    ) {
        $contactForms = $this->getParameter('tigris_contact');

        $name = $request->request->all('message')['form-name'] ?? $name; // Use for submitted form

        if (!isset($contactForms['forms'][$name])) {
            throw $this->createNotFoundException('Contact form not found');
        }

        $entity = (new Message())
            ->setForm($name)
        ;

        $session = $request->getSession();
        $dateDiff = new \DateTime();
        $dateDiff->sub(new \DateInterval('PT30M'));
        $formSubmitted = $session->get("contact-form-sended-{$name}", new \DateTime('1 month ago')) > $dateDiff;

        $form = $this->createForm(MessageType::class, $entity, ['method' => 'POST', 'action' => $this->generateUrl('tigris_contact_message_new')]);

        $form->add($arrayToFormService->createForm($contactForms['forms'][$name]['fields'], 'data'));
        $form->add('form-name', HiddenType::class, ['mapped' => false, 'data' => $name]);
        $this->addFormActions($form, false, 'button.send');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $session->set("contact-form-sended-{$name}", new \DateTime());

            // Notification
            $admins = $userRepository->findSuperAdmins();
            foreach ($admins as $admin) {
                $event = new NotificationEvent(null, $admin, [
                    'title' => 'contact.email.new_message',
                    'message' => 'contact.email.new_message',
                    'messageParams' => [],
                    'buttons' => [
                        ['label' => 'contact.email.button_label', 'route' => 'tigris_contact_admin_contact_index', 'routeParams' => []],
                    ],
                ]);

                $eventDispatcher->dispatch($event, Events::NOTIFY);
            }

            // Send mail to recipients
            try {
                $recipientsEmails = $configManager->getvalue('TigrisContactBundle.recipients_emails', []);
                $siteTitle = $configManager->getvalue('TigrisBaseBundle.title', '');
                $notificationMail = $configManager->getvalue('TigrisBaseBundle.noreply_mail', '');
                if ($recipientsEmails) {
                    $email = (new TemplatedEmail())
                        ->from(new Address($notificationMail, $siteTitle))
                        ->subject($translator->trans('contact.email.new_message'))
                        ->htmlTemplate('@TigrisContact/email/email.html.twig')

                        ->context([
                            'entity' => $entity,
                        ])
                    ;
                    foreach ($recipientsEmails as $address) {
                        $email->addTo($address);
                    }

                    $mailer->send($email);
                }
            } catch (\Exception $th) {
                $logger->error($th->getMessage());
            }
            

            if (!$request->isXmlHttpRequest()) {
                return $this->redirectToRoute('tigris_contact_message_index');
            }

            return new JsonResponse([
                'status' => 'success',
                'message' => $translator->trans('contact.form.success'),
            ]);
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $formSubmitted = false;
        }

        return $this->render('@TigrisContact/form.html.twig', ['form' => $form, 'formSubmitted' => $formSubmitted]);
    }
}
