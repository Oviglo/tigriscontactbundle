<?php

namespace Tigris\ContactBundle\Tests\Controller\Admin;

use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ContactBundle\Repository\MessageRepository;

class ContactControllerTest extends AbstractLoginWebTestCase
{
    public function testIndex(): void
    {
        $this->client->request('GET', '/admin/contact/');

        $this->assertResponseStatusCodeSame(302);

        $this->loginAdmin();

        $this->client->request('GET', '/admin/contact/');

        $this->assertResponseIsSuccessful();
    }

    public function testData(): void
    {
        $this->client->request('GET', '/admin/contact/data');

        $this->assertResponseStatusCodeSame(302);

        $this->loginAdmin();

        $this->client->request('GET', '/admin/contact/data');

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testShow(): void
    {
        $this->loginAdmin();

        $messageRepository = $this->getContainer()->get(MessageRepository::class);

        $message = $messageRepository->findLast();

        $this->client->request('GET', 'admin/contact/'.$message->getId().'/show/');

        $this->assertResponseIsSuccessful();
    }
}
